# Caratteristiche del bot

- Gestione accessi (captcha)
- Messaggio di benvenuto
- Regole
- Link del gruppo
- Reindirizzamenti e Alias
- Funzione di aiuto (Help)

---

- Gestione staff (ruoli con riferimenti multipli)
- Alias @admin
- Gestione utenti inattivi
- Visualizzazione dati degli iscritti
- Cancellazione dati degli iscritti (causa l'uscita dal gruppo)

---

- inviare messaggi a nome del bot
- fissare messaggi a nome del bot

---

- Gestione warn
- Gestione mute
- Gestione kick
- Gestione ban

---

- Multilingua
